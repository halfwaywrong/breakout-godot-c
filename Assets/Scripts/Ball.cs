using Godot;
using System;

public class Ball : Area2D
{
    [Export]
    private int speed = 50;
    private Vector2 initialPos;
    private Vector2 direction;

    private AnimationPlayer anim;
    private AudioStreamPlayer2D soundPaddle;
    private AudioStreamPlayer2D soundWalls;


    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        anim = (AnimationPlayer)GetNode("anim");
        soundPaddle = (AudioStreamPlayer2D)GetNode("soundPaddle");
        soundWalls = (AudioStreamPlayer2D)GetNode("soundWalls");

        initialPos = GetPosition();
        direction = new Vector2(0,-1);
        anim.Play("spin");

        Connect("body_entered", this, nameof(OnBodyEnter));
        Connect("area_entered", this, nameof(OnAreaEnter));
        
    }


    public override void _Process(float delta)
    {
        Position += direction * speed * delta;
    }

    public void IncreaseSpeed(int value){
        speed += value;
    }

    public void OnBodyEnter(PhysicsBody2D body)
    {   
        if (body.IsInGroup("Player")){

            Vector2 playerPos = body.GetPosition();
                       
            //compare the ball position with the player paddle position
            //then apply the angle
            //divide by 8 because the player sprite is 16px wide
            //ideally you'd want to get the player body size programmtically to remove any hardcoded magic numbers
            if (playerPos.y > Position.y){
                direction = new Vector2((Position.x - playerPos.x)/8, -direction.y);       
            }else{
                direction = new Vector2((Position.x - playerPos.x)/8, direction.y); 
            }

            direction = direction.Normalized();
            soundPaddle.Play();

        }
        else if (body.IsInGroup("Sides")){
            direction = new Vector2(-direction.x, direction.y);
            soundWalls.Play();
        }
        else if (body.IsInGroup("Top")){
            direction = new Vector2(direction.x, -direction.y);
            soundWalls.Play();
        }

    }

    public void OnAreaEnter(PhysicsBody2D body){
        if (body.IsInGroup("Blocks")){
            
            //Should check this conversion instead of just assuming type.
            Sprite block = (Sprite)body.GetParent();
            Vector2 blockPos = block.GetPosition();            
            bool under = false;
            bool left = false;
            bool hitSide = false;

            //the rest of the code determines which side of the block was hit
            //and then applies the direction accordingly
            
            //determine if the ball is over/under and left/right
            if (blockPos.y < Position.y){
                under = true;
            }
            if (blockPos.x > Position.x){
                left = true;
            }

            //compare difference between x and y to determine if it hit the top/bottom or sides.
            if (Math.Abs(blockPos.x - Position.x) > Math.Abs(block.Position.y - Position.y)){
                hitSide = true;
            }

            if (hitSide){
                if (left){
                    direction.x = -Math.Abs(direction.x);
                }
                else{
                    direction.x = Math.Abs(direction.x);
                }
            }else{
                if (under){
                    direction.y = Math.Abs(direction.y);
                }
                else{
                    direction.y = -Math.Abs(direction.y);
                }
            }
        }
    }
}
