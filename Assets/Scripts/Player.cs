using Godot;
using System;

public class Player : KinematicBody2D
{
    [Export]
    private int speed = 100;

    public override void _PhysicsProcess(float delta)
    {
        var motion = new Vector2();

        if (Input.IsActionPressed("move_left")){
            motion += new Vector2(-1,0);
		}
		if (Input.IsActionPressed("move_right")){
			motion += new Vector2(1,0);
		}

        motion = motion.Normalized() * speed;

        MoveAndSlide(motion);
    }
}
