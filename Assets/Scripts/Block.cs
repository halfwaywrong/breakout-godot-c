using Godot;
using System;

public class Block : Area2D
{
    [Export]
    protected int health = 1;

    private AudioStreamPlayer2D soundHit;
    private AudioStreamPlayer2D soundDestroy;
    
    public override void _Ready()
    {
        soundHit = (AudioStreamPlayer2D)GetNode("soundHit");
        soundDestroy = (AudioStreamPlayer2D)GetNode("soundDestroy");

        Connect("area_entered", this, nameof(OnAreaEnter));        
        GetNode("soundDestroy").Connect("finished", this, nameof(OnSoundDestroyFinish));
        
    }
    public void OnAreaEnter(PhysicsBody2D body)
    {
        if (body.GetName() == "Ball"){
            
            health--;

            if (health <= 0){
                var parent = GetParent();
                soundDestroy.Play();                
            }else{
                soundHit.Play();
            }
        }
    }

    public void OnSoundDestroyFinish(){
        var parent = GetParent();
        parent.QueueFree();
    }
}
