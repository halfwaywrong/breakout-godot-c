using Godot;
using System;

public class Game : Node2D
{

    [Export]
    private int speedIncrease = 5;

    private Node2D blocks;
    private Ball ball;
    private int blockCount;

    public override void _Ready(){
        blocks = (Node2D)GetNode("Blocks");
        ball = (Ball)GetNode("Ball");

        //In this example I've made the level via the GUI,
        //but you could easily build it programmatically here.

        //get the total number of blocks at level start
        blockCount = blocks.GetChildCount();

        //reduce the audio volume
        AudioServer.SetBusVolumeDb(AudioServer.GetBusIndex("Master"), -10);
    }

    public override void _Process(float delta){
        
        var curBlocks = blocks.GetChildCount();

        //if the ball has fallen down the bottom, restart the game
        if (ball.Position.y > GetViewport().GetVisibleRect().Size.y){
            GetTree().ReloadCurrentScene();
        }

        //if there are no more blocks left, restart the game
        if (curBlocks <= 0){
            GetTree().ReloadCurrentScene();
        }

        //if a block is destroyed, increase the speed
        if (blockCount != curBlocks){
            ball.IncreaseSpeed(speedIncrease);
            blockCount = curBlocks;
        }

    }


}
