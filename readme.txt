A simple 8-bit style Breakout clone using Godot 3.1 mono build. Made over the Easter break 2019.

Use either the arrow keys or 'A' and 'D' to move. Don't let the ball fall to the bottom. That's it!

Released under CC0 licence - please use this for whatever you like.
I encourage anyone that finds something useful here to follow me at twitter.com/HalfwayWrong or tumblr.com/halfwaywronggames and let me know about it!

Artwork was made by surt and can be found here - https://opengameart.org/content/breakdown

Everything else was made by me - HalfwayWrong Games aka Michael Henderson.

